package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {

    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
	private File file;
	private TupleDesc td;
    public HeapFile(File f, TupleDesc td) {
        // some code goes here
    	this.file = f;
    	this.td = td;
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        // some code goes here
        return file;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        // some code goes here
        return file.getAbsoluteFile().hashCode();
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
        return td;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // some code goes here
        int tableid = pid.getTableId();
        int pgNo = pid.pageNumber();
        int pageSize = BufferPool.getPageSize();
        byte[] data = HeapPage.createEmptyPageData();
        try{
        	FileInputStream in = new FileInputStream(file);
        	in.skip(pageSize * pgNo);
        	in.read(data);
        	in.close();
        	return new HeapPage(new HeapPageId(tableid, pgNo), data);
        }
        catch(FileNotFoundException e){
        	throw new IllegalArgumentException();
        }
        catch(IOException e){
        	throw new IllegalArgumentException();
        }
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        // some code goes here
        return (int)Math.ceil((file.length() / BufferPool.getPageSize()));
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        return null;
        // not necessary for lab1
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        // some code goes here
        return null;
        // not necessary for lab1
    }

    // see DbFile.java for javadocs
    private class HeapFileIterator implements DbFileIterator {
    	private TransactionId tid;              
        private int tableId;
        private int numPages;
        private Integer currPage;
        private Iterator<Tuple> iterator; 
        public HeapFileIterator(TransactionId tid) {
            this.tid = tid;
            this.tableId = getId();
            this.numPages = numPages();
            this.currPage = null;
            this.iterator = null;
        }
        @Override
        public boolean hasNext() throws DbException, TransactionAbortedException {
            if (currPage == null) return false;
            while (currPage < numPages - 1) {
                if (iterator.hasNext()) return true;
                else iterator = getIterator(++currPage);
            }
            return iterator.hasNext();
        }
        @Override
        public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
            if (hasNext()) return iterator.next();
            throw new NoSuchElementException();
        }
        @Override
        public void open() throws DbException, TransactionAbortedException {
            currPage = 0;
            iterator = getIterator(currPage);
        }        
        @Override
        public void close() {
            currPage = null;
            iterator = null;
        }
        @Override
        public void rewind() throws DbException, TransactionAbortedException {
            close();
            open();
        }
        private Iterator<Tuple> getIterator(int currPage)
                throws TransactionAbortedException, DbException {
            PageId pid = new HeapPageId(tableId, currPage);
            return ((HeapPage)Database.getBufferPool().getPage(tid, pid, Permissions.READ_ONLY)).iterator();
        }
    }

    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
        // some code goes here
        return new HeapFileIterator(tid);
    }

}

